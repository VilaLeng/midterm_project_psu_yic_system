import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sqlite_basic/database/database_helper.dart';
import 'package:sqlite_basic/model/profile_model.dart';
import 'dart:developer' as developer;

class AddProfile extends StatefulWidget {
  AddProfile();

  @override
  _AddProfileState createState() => _AddProfileState();
}

class _AddProfileState extends State<AddProfile> {
  var firstname = TextEditingController();
  var lastname = TextEditingController();
  var email = TextEditingController();
  var phone = TextEditingController();
  var _image;
  var imagePicker;

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Register to PSU YIC System',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 23,
              color: Colors.white70),
        ),
      ),
      body: Container(
        color: Colors.blueGrey,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    /*Padding(
                      padding: const EdgeInsets.all(25.25),
                      child: GestureDetector(
                        onTap: () async {
                          final image = await imagePicker.pickImage(
                            source: ImageSource.gallery,
                            imageQuality: 50,
                            //preferredCameraDevice: CameraDevice.front
                          );
                          setState(() {
                            _image = File(image.path);
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration: BoxDecoration(color: Colors.blueGrey),
                          child: _image != null
                              ? Image.file(
                                  _image,
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  decoration:
                                      BoxDecoration(color: Colors.white38),
                                  width: 200,
                                  height: 200,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),*/
                    buildTextfield('Enter Firstname', firstname),
                    buildTextfield('Enter Lastname', lastname),
                    buildTextfield('Enter Email', email),
                    buildTextfield('Enter Student ID', phone),
                    Padding(
                      padding: const EdgeInsets.all(25.25),
                      child: GestureDetector(
                        onTap: () async {
                          final image = await imagePicker.pickImage(
                            source: ImageSource.gallery,
                            imageQuality: 50,
                            //preferredCameraDevice: CameraDevice.front
                          );
                          setState(() {
                            _image = File(image.path);
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration: BoxDecoration(color: Colors.blueGrey),
                          child: _image != null
                              ? Image.file(
                                  _image,
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white38,
                                    borderRadius: BorderRadius.circular(18),
                                  ),
                                  width: 200,
                                  height: 200,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Add'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {
        if (title == 'Add') {
          await DatabaseHelper.instance.add(
            ProfileModel(
                firstname: firstname.text,
                lastname: lastname.text,
                email: email.text,
                phone: phone.text,
                image: _image.path),
          );
          setState(() {
            //firstname.clear();
            //selectedId = null;
          });

          Navigator.pop(context);
        } else
          Navigator.pop(context);
      },
      style: ElevatedButton.styleFrom(
        fixedSize: Size(139, 39),
        padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
        textStyle: const TextStyle(
          fontSize: 21,
          fontWeight: FontWeight.bold, color: Colors.white70,
        ),
      ),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
          color: Colors. white70,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white38,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
          ),
          contentPadding:
              EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
        ),
      ),
    );
  }
}
