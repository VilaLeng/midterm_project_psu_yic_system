


import 'package:sqlite_basic/Fragments/eventPage.dart';
import 'package:sqlite_basic/Fragments/homePage.dart';
import 'package:sqlite_basic/Fragments/notificationPage.dart';
import 'package:sqlite_basic/Fragments/profilePage.dart';


class pageRoutes {
  static const String home = homePage.routeName;

  static const String event = eventPage.routeName;
  static const String profile = profilePage.routeName;
  static const String notification = notificationPage.routeName;

}
