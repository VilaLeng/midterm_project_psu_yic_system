import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sqlite_basic/login/login.dart';
import 'package:sqlite_basic/pages/add_profile.dart';
import 'package:sqlite_basic/pages/edit_profile.dart';
import 'package:sqlite_basic/model/profile_model.dart';
import 'package:sqlite_basic/pages/show_profile.dart';
import 'package:sqlite_basic/routes/pageRoute.dart';
import 'package:sqlite_basic/widgets/createDrawerBodyItem.dart';
import 'package:sqlite_basic/widgets/createDrawerHeader.dart';
import 'database/database_helper.dart';
import 'pages/edit_profile.dart';

import 'package:flutter/cupertino.dart';

import 'Fragments/eventPage.dart';
import 'Fragments/homePage.dart';
import 'Fragments/notificationPage.dart';
import 'Fragments/profilePage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PSU YIC Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'PSU YIC System'),
      debugShowCheckedModeBanner: false,
      routes: {
        pageRoutes.home: (context) => homePage(),

        pageRoutes.event: (context) => eventPage(),
        pageRoutes.profile: (context) => profilePage(),
        pageRoutes.notification: (context) => notificationPage(),

      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int? selectedId;
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),

        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.app_registration),
              tooltip: 'Register to PSU YIC System',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddProfile()),
                ).then((value) {
                  //getAllData();
                  setState(() {});
                });
              },
            ),
          ),
        ],
      ),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Home Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => homePage())),
            ),
            // createDrawerHeader(),


            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'About Us',
              onTap: () =>
              //Navigator.pushReplacementNamed(context, pageRoutes.profile),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => profilePage())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'User Detail',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => eventPage())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'YIC News',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => notificationPage())),
            ),
          ],
        ),
      ),
      body: Center(
        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getGroceries(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  // ? Center(child: Text('No Profiles in List.'))
                  //it is the add start
                  ? Center(
                      child: ListView(
                      padding: EdgeInsets.zero,
                      children: <Widget>[
                       createDrawerHeader(),
                       /* Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(10),
                            child: const Text(
                              'Sign in',
                              style: TextStyle(fontSize: 20),
                            )),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextField(
                            controller: nameController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'User Name',
                            ),
                          ),
                        ),*/
                       /* Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextField(
                            obscureText: true,
                            controller: passwordController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Password',
                            ),
                          ),
                        ),*/
                       /* TextButton(
                          onPressed: () {
                            //forgot password screen
                          },
                          child: const Text('Forgot Password',),
                        ),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: ElevatedButton(
                              child: const Text('Login'),
                              onPressed: () {
                                print(nameController.text);
                                print(passwordController.text);
                              },
                            )
                        ),
                        Row(
                          children: <Widget>[
                            const Text('Does not have account?'),
                            TextButton(
                              child: const Text(
                                'Sign in',
                                style: TextStyle(fontSize: 20),
                              ),
                              onPressed: () {
                                //signup screen
                              },
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                        ),
*/
                        createDrawerBodyItem(
                          icon: Icons.home,
                          text: 'Home Page',
                          onTap: () => Navigator.pushReplacementNamed(
                              context, pageRoutes.home),
                        ),
                        // createDrawerHeader(),

                        createDrawerBodyItem(
                          icon: Icons.account_circle,
                          text: 'About Us',
                          onTap: () => Navigator.pushReplacementNamed(
                              context, pageRoutes.profile),
                        ),

                        createDrawerBodyItem(
                          //icon: Icons.contact_phone,
                          icon: Icons.emoji_objects,
                          text: 'User Detail',
                          onTap: () => Navigator.pushReplacementNamed(
                              context, pageRoutes.event),
                        ),
                        createDrawerBodyItem(
                          icon: Icons.article,
                          text: 'YIC News',
                          onTap: () => Navigator.pushReplacementNamed(
                              context, pageRoutes.notification),
                        ),
                        TextButton(
                          onPressed: () {
                            //forgot password screen
                          },
                          child: const Text('The PSU YIC Membership System',style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),),
                        ),
                        Container(
                            height: 47,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: ElevatedButton(
                              child: const Text('Login',style: TextStyle(
                                  color: Colors.white70,
                                  fontSize: 21,
                                  fontWeight: FontWeight.bold),),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            profilePage()));
                              },
                            )
                        ),
                        Row(
                          children: <Widget>[
                            const Text('Does not have account?',style: TextStyle(
                                color: Colors.blueGrey,
                                fontSize: 21,
                                fontWeight: FontWeight.bold),),
                            TextButton(
                              child: const Text(
                                'Sign In',
                                style: TextStyle(fontSize: 20, color: Colors.lightBlue,fontWeight: FontWeight.bold),
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => AddProfile()),
                                ).then((value) {
                                  //getAllData();
                                  setState(() {});
                                });
                              },
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                        ),

                        /* ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddProfile()),
                            ).then((value) {
                              //getAllData();
                              setState(() {});
                              label: const Text('Plus One');
                              icon: const Icon(Icons.plus_one);
                            });
                          }, child: null,
                        ),*/
                      ],
                    )) //it is the adding end
                  : ListView(
                      children: snapshot.data!.map((grocery) {
                        return Center(
                          child: Card(
                            color: selectedId == grocery.id
                                ? Colors.white70
                                : Colors.white,
                            child: ListTile(
                              title: Text(
                                '${grocery.firstname} ${grocery.lastname} (${grocery.phone})',
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blueGrey),
                              ),
                              subtitle: Text(
                                '${grocery.email}',
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blueGrey),
                              ),
                              leading: CircleAvatar(
                                  minRadius: 15,
                                  maxRadius: 37,
                                  backgroundImage: FileImage(
                                    File(grocery.image),
                                  )),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit),
                                    iconSize: 25,
                                    color: Colors.blueGrey,
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                EditProfile(grocery)),
                                      ).then((value) {
                                        setState(() {});
                                      });
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.cancel),
                                    iconSize: 25,
                                    color: Colors.blueGrey,
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: new Text(
                                                "Do you want to leave the PSU YIC System?"),
                                            // content: new Text("Please Confirm"),
                                            actions: [
                                              new TextButton(
                                                onPressed: () {
                                                  DatabaseHelper.instance
                                                      .remove(grocery.id!);
                                                  setState(() {
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: new Text("Ok"),
                                              ),
                                              Visibility(
                                                visible: true,
                                                child: new TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: new Text("Cancel"),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              onTap: () {
                                var profileid = grocery.id;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ShowProfile(id: profileid)));

                                setState(() {
                                  print(grocery.image);
                                  if (selectedId == null) {
                                    //firstname.text = grocery.firstname;
                                    selectedId = grocery.id;
                                  } else {
                                    // textController.text = '';
                                    selectedId = null;
                                  }
                                }

                                );
                              },

                              onLongPress: () {
                                setState(() {
                                  DatabaseHelper.instance.remove(grocery.id!);
                                });
                              },
                            ),
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
    );
  }
}
