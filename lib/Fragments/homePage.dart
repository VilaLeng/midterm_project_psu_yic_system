import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sqlite_basic/Fragments/eventPage.dart';
import 'package:sqlite_basic/Fragments/profilePage.dart';
import 'package:sqlite_basic/database/database_helper.dart';
import 'package:sqlite_basic/main.dart';
import 'package:sqlite_basic/model/profile_model.dart';
import 'package:sqlite_basic/navigationDrawer/navigationDrawer.dart';
import 'package:sqlite_basic/pages/add_profile.dart';
import 'package:sqlite_basic/pages/edit_profile.dart';
import 'package:sqlite_basic/pages/show_profile.dart';
import 'package:sqlite_basic/routes/pageRoute.dart';
import 'package:sqlite_basic/widgets/createDrawerBodyItem.dart';
import 'package:sqlite_basic/widgets/createDrawerHeader.dart';

import 'notificationPage.dart';

class homePage extends StatelessWidget {
  static const String routeName = '/homePage';

  @override
  Widget build(BuildContext context) {
    var pageRoutes;
    return new Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      // drawer: navigationDrawer(),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Home Page',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.home),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => homePage())),
            ),
            // createDrawerHeader(),

            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'About Us',
              onTap: () =>
                  //Navigator.pushReplacementNamed(context, pageRoutes.profile),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => profilePage())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'User Detail',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => eventPage())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'YIC News',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => notificationPage())),
            ),
          ],
        ),
      ),
      // body: Center(child: Text("This is home page"))
      body: Column(
        children: [
          Padding(
            padding:
            const EdgeInsets.only(top: 25, left: 15, right: 15, bottom: 0),
            child: Text(
              'Welcome to PSU Young Investor Club Membership Register System',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Row(
            children: [
              // CircleAvatar(
              //   radius:168,
              //   backgroundImage: AssetImage('assets/images/japan.jpg'),
              // ),
              Image.asset(
                'assets/images/psunew.jpg',
                width: 392,
                height: 353,
                scale: 0.7,
              ),
            ],
          ),
          //Text('Welcome to Home Page')
         /* Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 30),
            child: TextField(
              *//*maxLines: null,
              expands: true,
              keyboardType: TextInputType.multiline,*//*
              //controller: myController_pwd,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              decoration: InputDecoration(
                hintText:
                    'Welcome to PSU Young Investor \n Club Membership Register System',
              ),
            ),
          ),*/
          Padding(
            padding:
                const EdgeInsets.only(top: 10, left: 15, right: 15, bottom: 30),
            child: Text(
              'This System if for the students of PSU Trang Campus who are interested in the'
                  'Financial and Stock investment to join, to work and to share each other the knowledge and experience'
                  ' for becoming the group of young investors.',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
         /* Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 30),
            child: TextField(
              //controller: myController_pwd,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
              decoration: InputDecoration(
                hintText: 'Nothing is Perfect on the First Time.',
              ),
            ),
          ),*/
          ElevatedButton.icon(
            onPressed: () {
              //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
              //
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MyHomePage(
                            title: 'PSU YIC System',
                          )));
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            label: Text("Go Back"),
            style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
          ),
        ],
      ),
    );
  }
}
