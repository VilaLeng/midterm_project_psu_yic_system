
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sqlite_basic/Fragments/eventPage.dart';
import 'package:sqlite_basic/database/database_helper.dart';
import 'package:sqlite_basic/model/profile_model.dart';
import 'package:sqlite_basic/navigationDrawer/navigationDrawer.dart';
import 'package:sqlite_basic/pages/add_profile.dart';
import 'package:sqlite_basic/pages/edit_profile.dart';
import 'package:sqlite_basic/pages/show_profile.dart';
import 'package:sqlite_basic/widgets/createDrawerBodyItem.dart';
import 'package:sqlite_basic/widgets/createDrawerHeader.dart';
import 'dart:io';
import '../main.dart';
import 'homePage.dart';
import 'notificationPage.dart';

class profilePage extends StatelessWidget {
  static const String routeName = '/profilePage';

  @override
  Widget build(BuildContext context) {
    var pageRoutes;
    return new Scaffold(
      appBar: AppBar(
        title: Text("About Us Page"),
      ),
      //drawer: navigationDrawer(),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Home Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => homePage())),
            ),
            // createDrawerHeader(),


            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'About Us',
              onTap: () =>
              //Navigator.pushReplacementNamed(context, pageRoutes.profile),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => profilePage())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'User Detail',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => eventPage())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'YIC News',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => notificationPage())),
            ),
          ],
        ),
      ),
      //  body: Center(child: Text("This is profile page"))
      body: Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: 20,
        ),
        color: Colors.grey[200],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 38,
                    backgroundImage: AssetImage('assets/images/vila.jpg'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Mr. Vila Leng',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Software Developer',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.flutter_dash,
                    size: 58,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'My name is Mr. Vila Leng, you can call me Win, my ID is 6250110030. I am a junior student at Prince of Songkla University'
                      ', Trang Campus. My Major is Information and Computer Management.'
                      'I will be a skillful Software Developer.',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 38,
                    backgroundImage: AssetImage('assets/images/mina.jpg'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Miss. Naseefah Meenakong',
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Computer Programmer',
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.computer_sharp,
                    size: 58,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'My name is Miss. Naseefah Meenakong , you can call me Miinaa, my ID is 6250110025. I am a junior student at Prince of Songkla University'
                      ', Trang Campus. My Major is Information and Computer Management.'
                      'I want to be a Computer Programmer.',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              /*Padding(
                padding: EdgeInsets.only(
                    left: 0,
                    right: 130,
                    top: 10,
                    bottom: 10
                ),
                child: Text(
                  'My Contact Information:',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
              ),*/
              /*Padding(
                padding: EdgeInsets.only(
                    left: 0,
                    right: 205,
                    top: 10,
                    bottom: 10
                ),
                child: Text(
                  'Tel: 092506567',
                  style: TextStyle(
                      fontSize: 15,
                      //fontWeight: FontWeight.bold,
                      color: Colors.blueGrey[69]),
                ),
              ),*/
             /* Padding(
                padding: EdgeInsets.only(
                    left: 0,
                    right: 38,
                    top: 10,
                    bottom: 10
                ),
                child: Text(
                  'Email Adress: 6250110030@gmail.com',
                  style: TextStyle(
                      fontSize: 15,
                      //fontWeight: FontWeight.bold,
                      color: Colors.blueGrey[69]),
                ),
              ),*/
             /* Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),*/
              ElevatedButton.icon(
                onPressed: () {
                  //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                  //
                  Navigator.push(context,
                      MaterialPageRoute(
                          builder: (context) => MyHomePage(title: 'PSU YIC System',)));

                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
                label: Text("Go Back"),
                style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
