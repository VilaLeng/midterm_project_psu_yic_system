import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sqlite_basic/Fragments/profilePage.dart';
import 'package:sqlite_basic/navigationDrawer/navigationDrawer.dart';
import 'package:sqlite_basic/pages/show_profile.dart';
import 'package:sqlite_basic/widgets/createDrawerBodyItem.dart';
import 'package:sqlite_basic/widgets/createDrawerHeader.dart';

import '../main.dart';
import 'eventPage.dart';
import 'homePage.dart';

class notificationPage extends StatelessWidget {
  static const String routeName = '/notificationPage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("YIC News"),
        ),
        drawer: Drawer(
          backgroundColor: Colors.blueGrey[200],
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              createDrawerHeader(),
              createDrawerBodyItem(
                icon: Icons.home,
                text: 'Home Page',
                onTap: () =>
                // Navigator.pushReplacementNamed(context, pageRoutes.home),
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => homePage())),
              ),
              // createDrawerHeader(),

              createDrawerBodyItem(
                icon: Icons.account_circle,
                text: 'About Us',
                onTap: () =>
                //Navigator.pushReplacementNamed(context, pageRoutes.profile),
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => profilePage())),
              ),

              createDrawerBodyItem(
                //icon: Icons.contact_phone,
                icon: Icons.emoji_objects,
                text: 'User Detail',
                onTap: () =>
                // Navigator.pushReplacementNamed(context, pageRoutes.event),
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => eventPage())),
              ),
              createDrawerBodyItem(
                //icon: Icons.contact_phone,
                icon: Icons.article,
                text: 'YIC News',
                onTap: () =>
                // Navigator.pushReplacementNamed(context, pageRoutes.event),
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => notificationPage())),
              ),
            ],
          ),),
        body: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 15,
          mainAxisSpacing: 15,
          crossAxisCount: 2,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text(
                "Start-Up Investment Plan",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.blueGrey[600],
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Stock Investment Plan',style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),),
              color: Colors.blueGrey[200],
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Realestate Investment Plan ',style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),),
              color: Colors.blueGrey[300],
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Block Chain Invesment Plan',style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),),
              color: Colors.blueGrey[400],
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Cryptocurrency Investment Plan',style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),),
              color: Colors.blueGrey[500],
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('NFT Investment Plan',style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),),
              color: Colors.blueGrey[600],
            ),
            ElevatedButton.icon(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => MyHomePage(title: 'PSU YIC System',)));


              },
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.lightBlue,
              ),
              label: Text("Go Back",style: TextStyle(
                  color: Colors.lightBlue,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),),
              style: ElevatedButton.styleFrom(primary: Colors.blueGrey[100]),
            ),

          ],

        )


    );
  }
}
