import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sqlite_basic/Fragments/profilePage.dart';
import 'package:sqlite_basic/database/database_helper.dart';
import 'package:sqlite_basic/model/profile_model.dart';
import 'package:sqlite_basic/navigationDrawer/navigationDrawer.dart';
import 'package:sqlite_basic/pages/add_profile.dart';
import 'package:sqlite_basic/pages/edit_profile.dart';
import 'package:sqlite_basic/pages/show_profile.dart';
import 'package:sqlite_basic/widgets/createDrawerBodyItem.dart';
import 'package:sqlite_basic/widgets/createDrawerHeader.dart';
import 'dart:io';
import '../main.dart';
import 'homePage.dart';
import 'notificationPage.dart';

class eventPage extends StatelessWidget {
  static const String routeName = '/eventPage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("User Detail Page"),
      ),
      //drawer: navigationDrawer(),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Home Page',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.home),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => homePage())),
            ),
            // createDrawerHeader(),

            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'About Us',
              onTap: () =>
                  //Navigator.pushReplacementNamed(context, pageRoutes.profile),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => profilePage())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'User Detail',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.event),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => eventPage())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'YIC News',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.event),
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => notificationPage())),
            ),
          ],
        ),
      ),
      /*body: Center(
            child: Text(
              "Good Luck, Please Have a Nice Day.",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 21,
                color: Colors.red,
                backgroundColor: Colors.lime,
              ),
            )

        )*/
      /* body: Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: 20,
        ),
        color: Colors.grey[200],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [

              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: 0,
                    right: 130,
                    top: 10,
                    bottom: 10
                ),
                child: Text(
                  'My Contact Information:',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: 0,
                    right: 205,
                    top: 10,
                    bottom: 10
                ),
                child: Text(
                  'Tel: 092506567',
                  style: TextStyle(
                      fontSize: 15,
                      //fontWeight: FontWeight.bold,
                      color: Colors.blueGrey[69]),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: 0,
                    right: 38,
                    top: 10,
                    bottom: 10
                ),
                child: Text(
                  'Email Adress: 6250110030@gmail.com',
                  style: TextStyle(
                      fontSize: 15,
                      //fontWeight: FontWeight.bold,
                      color: Colors.blueGrey[69]),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              ElevatedButton.icon(
                onPressed: () {
                  //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                  //
                  Navigator.push(context,
                      MaterialPageRoute(
                          builder: (context) => MyHomePage(title: 'PSU YIC System',)));

                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
                label: Text("Go Back"),
                style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
              ),
             */ /* Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),*/ /*
              */ /*ElevatedButton.icon(
                onPressed: () {
                  //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                  //
                  Navigator.push(context,
                      MaterialPageRoute(
                          builder: (context) => ));

                },
                icon: Icon(
                  Icons.check,
                  color: Colors.white,
                ),
                label: Text("See Member Detail"),
                style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
              ),*/ /*
            ],
          ),
        ),
      ),
      */
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.bookmark),iconColor: Colors.blueGrey,
                  title: const Text('The Members Ship of PSU YIC System',
                      style: TextStyle(
                        color: Colors.blueGrey,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      )),
                  subtitle: Text(
                    'We are all the YIC family.',
                    style: TextStyle(
                      color: Colors.blueGrey,
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'The priority task of the PSU YIC System is makeing the members of the club to get more comfortable'
                    'way of communication and receiving useful information and making the new students who'
                    'interesting in investment to be the member.',
                    style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: 13,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.start,
                  children: [
                    FlatButton(
                     // textColor: const Color(0xFF051D74),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(
                                builder: (context) => notificationPage()));
                        // Perform some action
                      },
                      child: const Text('Technology Information',style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),),
                    ),
                    FlatButton(
                    //  textColor: const Color(0xFF051D74),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(
                                builder: (context) => notificationPage()));
                        // Perform some action
                      },
                      child: const Text(
                        'Investment Information',
                        style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ],
                ),
                Image.asset('assets/images/psuold.jpg'),
                SizedBox(
                  height: 16,
                ),

                Image.asset('assets/images/psunew.jpg'),
                ElevatedButton.icon(
                  onPressed: () {
                    //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                    //
                    Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) => MyHomePage(title: 'PSU YIC System',)));

                  },
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  label: Text("Go Back"),
                  style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
                ),

              ],

            ),
          ),
         /* AlertDialog(
            title: Text('Reset settings?'),
            content: Text('This will reset your device to its default factory settings.'),
            actions: [
              FlatButton(
                textColor: Color(0xFF6200EE),
                onPressed: () {},
                child: Text('CANCEL'),
              ),
              FlatButton(
                textColor: Color(0xFF6200EE),
                onPressed: () {},
                child: Text('ACCEPT'),
              ),
            ],
          )*/
        ],
      ),
    );
  }
}
